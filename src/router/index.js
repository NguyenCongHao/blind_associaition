import {
    createRouter,
    createWebHistory
} from "vue-router";

import store from "../store";
const routes = [{
        path: "/",
        name: "home",
        redirect: "/ ",
        component: () =>
            import (
                "../home"
            ),

        children: [{
                path: "/ ",
                name: "trangChu",
                component: () =>
                    import (
                        "../views/trangChu.vue"
                    )
            },
            {
                path: "/gioi-thieu",
                name: "gioiThieu",
                component: () =>
                    import (
                        "../views/gioiThieu.vue"
                    )
            },
            {
                path: "/thu-ngo",
                name: "index-thuNgo",
                redirect: "/thu-ngo/danh-sach-thu-ngo",
                component: () =>
                    import (
                        "../views/thuNgo/index.vue"
                    ),
                children: [{
                        path: "danh-sach-thu-ngo",
                        name: "thuNgo",
                        component: () =>
                            import (
                                "../views/thuNgo/thuNgo.vue"
                            )
                    },
                    {
                        path: "chi-tiet-thu-ngo/:thuNgoId",
                        name: "chiTietThuNgo",
                        component: () =>
                            import (
                                "../views/thuNgo/chiTiet.vue"
                            )
                    }
                ]
            },
            {
                path: "/tam-diem",
                name: "index-tamDiem",
                redirect: "/tam-diem/danh-sach-tam-diem",
                component: () =>
                    import (
                        "../views/tamDiem/index.vue"
                    ),
                children: [{
                        path: "danh-sach-tam-diem",
                        name: "tamDiem",
                        component: () =>
                            import (
                                "../views/tamDiem/tamDiem.vue"
                            )
                    },
                    {
                        path: "chi-tiet-tam-diem/:tamDiemId",
                        name: "chiTietTamDiem",
                        component: () =>
                            import (
                                "../views/tamDiem/chiTiet.vue"
                            )
                    }
                ]
            },
            {
                path: "/ket-noi-yeu-thuong",
                name: "index-ketNoiYeuThuong",
                redirect: "/ket-noi-yeu-thuong/danh-sach-ket-noi-yeu-thuong",
                component: () =>
                    import (
                        "../views/ketNoiYeuThuong/index.vue"
                    ),
                children: [{
                        path: "danh-sach-ket-noi-yeu-thuong",
                        name: "ketNoiYeuThuong",
                        component: () =>
                            import (
                                "../views/ketNoiYeuThuong/ketNoiYeuThuong.vue"
                            )
                    },
                    {
                        path: "chi-tiet-ket-noi-yeu-thuong/:knytId",
                        name: "chiTietKetNoiYeuThuong",
                        component: () =>
                            import (
                                "../views/ketNoiYeuThuong/chiTiet.vue"
                            )
                    }
                ]
            },
            {
                path: "/nha-hao-tam",
                name: "index-nhaHaoTam",
                redirect: "/nha-hao-tam/danh-sach-nha-hao-tam",
                component: () =>
                    import (
                        "../views/nhaHaoTam/index.vue"
                    ),
                children: [{
                        path: "danh-sach-nha-hao-tam",
                        name: "nhaHaoTam",
                        component: () =>
                            import (
                                "../views/nhaHaoTam/nhaHaoTam.vue"
                            )
                    },
                    {
                        path: "chi-tiet-nha-hao-tam/:nhtId",
                        name: "chiTietNhaHaoTam",
                        component: () =>
                            import (
                                "../views/nhaHaoTam/chiTiet.vue"
                            )
                    }
                ]
            },
            {
                path: "/lien-he",
                name: "lienHe",
                component: () =>
                    import (
                        "../views/lienHe.vue"
                    )
            },
        ]
    },

    {
        path: "/admin",
        name: "admin",
        redirect: "/admin/login",
        component: () =>
            import (
                "../views/admin/index.vue"
            ),
        children: [{
                path: "login",
                name: "login",
                component: () =>
                    import (
                        "../views/admin/login.vue"
                    ),
            },
            {
                path: "trang-chu-admin",
                name: "trangChuAdmin",
                redirect: "/admin/trang-chu-admin/lien-he/danh-sach",
                component: () =>
                    import (
                        "../views/admin/index-admin.vue"
                    ),
                meta: {
                    requiresAuth: true
                },
                props: true,
                children: [{
                        path: "lien-he",
                        name: "index-lienHe",
                        redirect: "lien-he/danh-sach",
                        component: () =>
                            import (
                                "../views/admin/lienHe/index.vue"
                            ),
                        children: [{
                            path: "danh-sach",
                            name: "lienHeAdmin",
                            component: () =>
                                import (
                                    "../views/admin/lienHe/lienHe-admin.vue"
                                )
                        }, ]
                    },
                    {
                        path: "bai-dang",
                        name: "index-baiDang",
                        redirect: "/bai-dang/danh-sach",
                        component: () =>
                            import (
                                "../views/admin/baiDang/index.vue"
                            ),
                        children: [{
                                path: "danh-sach",
                                name: "danhSachBaiDang",
                                component: () =>
                                    import (
                                        "../views/admin/baiDang/baiDang-admin.vue"
                                    )
                            },
                            {
                                path: "thu-ngo",
                                name: "danhSachBaiDangThuNgo",
                                component: () =>
                                    import (
                                        "../views/admin/baiDang/danhSachBaiDangThuNgo.vue"
                                    )
                            },
                            {
                                path: "tam-diem",
                                name: "danhSachBaiDangTamDiem",
                                component: () =>
                                    import (
                                        "../views/admin/baiDang/danhSachBaiDangTamDiem.vue"
                                    )
                            },
                            {
                                path: "ket-noi-yeu-thuong",
                                name: "danhSachBaiDangKnyt",
                                component: () =>
                                    import (
                                        "../views/admin/baiDang/danhSachBaiDangKnyt.vue"
                                    )
                            },
                            {
                                path: "nha-hao-tam",
                                name: "danhSachBaiDangNht",
                                component: () =>
                                    import (
                                        "../views/admin/baiDang/danhSachBaiDangNht.vue"
                                    )
                            },
                            {
                                path: "them-moi",
                                name: "themBaiDang",
                                component: () =>
                                    import (
                                        "../views/admin/baiDang/themBaiDang.vue"
                                    )
                            },
                            {
                                path: "chi-tiet/:idPost",
                                name: "chiTietBaiDang",
                                component: () =>
                                    import (
                                        "../views/admin/baiDang/chiTietBaiDang.vue"
                                    )
                            },
                            {
                                path: "cap-nhat/:idPost",
                                name: "capNhatBaiDang",
                                component: () =>
                                    import (
                                        "../views/admin/baiDang/capNhatBaiDang.vue"
                                    )
                            }
                        ]
                    },
                    {
                        path: "gioi-thieu",
                        name: "index-gioiThieu",
                        redirect: "gioi-thieu/danh-sach",
                        component: () =>
                            import (
                                "../views/admin/gioiThieu/index.vue"
                            ),
                        children: [{
                                path: "danh-sach",
                                name: "gioiThieuAdmin",
                                component: () =>
                                    import (
                                        "../views/admin/gioiThieu/gioiThieu-admin.vue"
                                    )
                            },
                            {
                                path: "them-moi",
                                name: "themGioiThieu",
                                component: () =>
                                    import (
                                        "../views/admin/gioiThieu/themMoi.vue"
                                    )
                            },
                            {
                                path: "cap-nhat/:idIntroduce",
                                name: "capNhatGioiThieu",
                                component: () =>
                                    import (
                                        "../views/admin/gioiThieu/capNhatGioiThieu.vue"
                                    )
                            },
                        ]
                    },

                    {
                        path: "tu-van",
                        name: "index-DStuVan",
                        redirect: "tu-van/danh-sach",
                        component: () =>
                            import (
                                "../views/admin/tuVan/index.vue"
                            ),
                        children: [{
                                path: "danh-sach",
                                name: "DStuVan",
                                component: () =>
                                    import (
                                        "../views/admin/tuVan/danhSach.vue"
                                    )
                            },

                        ]
                    },
                ]
            }
        ]
    },



];

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes,
});


router.beforeEach((to, from, next) => {
    const token = localStorage.getItem("accessToken");
    if (to.matched.some(record => record.meta.requiresAuth)) {
        if (store.getters.isLoggedIn) {
            if (!token || token === "") {
                this.$store.dispatch("doLogout").then(() => {
                    window.location.href = "/admin";
                });
            }
            next();
            // return;
        }
        next("/");
    } else {
        next();
    }
});

export default router;